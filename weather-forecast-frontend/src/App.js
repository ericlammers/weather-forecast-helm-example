import React, { useState, useEffect } from 'react';
import { version } from "../package.json";
import { getTheApiBaseUrl } from './url-utils';
import Config from './config.json';
import './App.css';

const App = () => {
  const [todaysWeatherForecast, setTodaysWeatherForecast] = useState("");
  const [tomorrowsWeatherForecast, setTomorrowsWeatherForecast] = useState("");

  useEffect(() => {
    const fetchData = async () => {
      const response = await fetch(`${getTheApiBaseUrl()}/weatherforecast/today`);
      const data = await response.json();
      setTodaysWeatherForecast(data.summary);
    }

    fetchData();
  }, []);

  useEffect(() => {
    const fetchData = async () => {
      const response = await fetch(`${getTheApiBaseUrl()}/weatherforecast/tomorrow`);
      const data = await response.json();
      setTomorrowsWeatherForecast(data.summary);
    }

    fetchData();
  }, []);

  

  const renderTodaysWeather = () => {
    if(!todaysWeatherForecast) {
      return <span className="loading">Loading todays weather...</span>
    }

    return (
      <div style={{ backgroundColor: Config.PanelColor }} className="text-container">
        <span className="weather-sub-title">The weather today is:</span>
        <span className="weather-forecast">{todaysWeatherForecast}</span>
      </div>
    );
  }

  const renderTomorrowsWeather = () => {
    if(!tomorrowsWeatherForecast) {
      return "";
    }

    return (
      <div style={{ backgroundColor: Config.PanelColor }} className="text-container">
        <span className="weather-sub-title">The weather tomorrow is:</span>
        <span className="weather-forecast">{tomorrowsWeatherForecast}</span>
      </div>
    );
  }

  return (
    <div className="app">
      <div className="header">
        <span className="app-name">Weather Forecast App</span>
        <span className="version">Version: {version}</span>
      </div>
      <div className="content">
        {renderTodaysWeather()}
        {renderTomorrowsWeather()}
      </div>
    </div>
  );
}

export default App;
