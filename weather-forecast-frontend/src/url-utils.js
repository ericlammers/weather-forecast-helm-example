import Config from './config.json';

export const getTheApiBaseUrl = () => {
   return window.location.protocol + '//' + window.location.hostname + ":" + Config.ApiPort;
}