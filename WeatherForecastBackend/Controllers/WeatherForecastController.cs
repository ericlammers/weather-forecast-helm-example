﻿using System;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace TodoBackend.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class WeatherForecastController : ControllerBase
    {
        private static readonly string[] Summaries = new[]
        {
            "Freezing", "Chilly", "Cool", "Mild", "Warm", "Balmy", "Hot", "Sweltering", "Scorching"
        };

        private readonly ILogger<WeatherForecastController> _logger;

        public WeatherForecastController(ILogger<WeatherForecastController> logger)
        {
            _logger = logger;
        }

        [HttpGet("today")]
        public WeatherForecast GetTodaysWeatherForecast()
        {
            return GetWeatherForecast();
        }

        [HttpGet("tomorrow")]
        public WeatherForecast GetTomorrowsWeatherForecast()
        {
            return GetWeatherForecast();
        }

        private WeatherForecast GetWeatherForecast() {
            _logger.LogInformation("Recieved a weather forecast request.");

            var rng = new Random();

            return new WeatherForecast
            {
                Summary = Summaries[rng.Next(Summaries.Length)]
            };
        }
    }
}
