{{- define "weather-forecast.chart" -}}
{{- printf "%s-%s" .Chart.Name .Chart.Version | replace "+" "_" | trunc 63 | trimSuffix "-" }}
{{- end }}

{{- define "weather-forecast.backend-deployment" -}}
{{- printf "%s-backend-deployment" .Chart.Name }}
{{- end }}

{{- define "weather-forecast.backend-service" -}}
{{- printf "%s-backend-service" .Chart.Name }}
{{- end }}

{{- define "weather-forecast.backend-appsettings-configmap" -}}
{{- printf "%s-backend-appsettings-configmap" .Chart.Name }}
{{- end }}

{{- define "weather-forecast.backend" -}}
{{- printf "%s-backend" .Chart.Name }}
{{- end }}

{{- define "weather-forecast.backend-image" -}}
{{- printf "%s/%s:%s" .Values.backend.deployment.image.repository .Values.backend.deployment.image.name .Values.backend.deployment.image.tag }}
{{- end }}

{{- define "weather-forecast.backend-appsettings-configmap-checksum" -}}
{{ include (print $.Template.BasePath "/backend-appsettings-configmap.yaml") . | sha256sum }}
{{- end }}

{{- define "weather-forecast.frontend-deployment" -}}
{{- printf "%s-frontend-deployment" .Chart.Name }}
{{- end }}

{{- define "weather-forecast.frontend-service" -}}
{{- printf "%s-frontend-service" .Chart.Name }}
{{- end }}

{{- define "weather-forecast.frontend-config-configmap" -}}
{{- printf "%s-frontend-config-configmap" .Chart.Name }}
{{- end }}

{{- define "weather-forecast.frontend" -}}
{{- printf "%s-frontend" .Chart.Name }}
{{- end }}

{{- define "weather-forecast.frontend-image" -}}
{{- printf "%s/%s:%s" .Values.frontend.deployment.image.repository .Values.frontend.deployment.image.name .Values.frontend.deployment.image.tag }}
{{- end }}

{{- define "weather-forecast.frontend-config-configmap-checksum" -}}
{{ include (print $.Template.BasePath "/frontend-config-configmap.yaml") . | sha256sum }}
{{- end }}

