# Helm 

## Kubernetes Overview

- Kubernetes is a container-orchestration system 

- Helps automate the deployment, scaling and management of containerized applications

- Kubernetes provides a set of objects that can be used to describe the state of cluster

- Once we create the objects in the cluster Kubernetes will actively work to ensure the cluster always contains those objects

- Some of the core objects in Kubernetes are:
  - Pod: Runs one or more containers, typically one container but there are some exceptions
  - Deployment: Manages a set of pods
  - Services: Allows for communication between pods in a cluster as well as the option to expose pods externally

- To create Kubernets objects in a cluster we can use the command line or yaml files

### Basic Kubernetes Deployment
```yaml
# deployment.yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: weather-forecast-backend-deployment
  labels:
    app: weather-forecast-backend
spec:
  replicas: 2
  selector:
    matchLabels:
      app: weather-forecast-backend
  template:
    metadata:
      labels:
        app: weather-forecast-backend
    spec:
      containers:
      - name: weather-forecast-backend
        image: docker.io/ericlammers/weather-forecast-backend:dev
        ports:
        - containerPort: 80
        env:
        - name: ASPNETCORE_LOGGING__LOGLEVEL__DEFAULT
          value: "Debug"
        - name: ASPNETCORE_ENVIRONMENT
          value: "Development"

# service.yaml
apiVersion: v1
kind: Service
metadata:
  name: weather-forecast-backend-service
spec:
  type: ClusterIP
  ports:
    - port: 5000
      targetPort: 80
      protocol: TCP
      name: http
  selector:
    app: weather-forecast-backend
```

### Challenges
- Hard code values which need to be updated per environment and/or per version 
  - image tag
  - log level
  - asp.net environment

- No variables for commonly used values
  - container port
  - application name

- Becomes hard to manage as the number of objects in the release increases
  - adding a front end requires a new deployment and service
  - may need an ingress controller
  - config maps for mounting configuration into pods

## Helm Overview
- A package manager for kubernetes

- Benefits:
  - Deploy or upgrade a set of kubernetes resources in a single step
  - Provides configurability of kubernetes resources
    - One helm chart can be used for multiple different environments
  - Handles release management, upgrade and rollbacks of the entire release 

## Helm chart
- The core concept in helm is the helm chart

- A chart is a collection of files that describes a set of kubernetes resources

- Charts can be used for simple deploys or more complex deploys

- Helm charts use templates to create the kubernetes objects 

## Chart structure

```
weather-forecast/
  chart.yaml
  values.yaml
  .helmignore
  templates/
    _helpers.tpl
    deployment.yaml
    service.yaml
    NOTES.txt
```

- Here is roughly the chart structure helm expects

- The directory name is the name of chart

### chart.yaml
```yaml
# chart.yaml

apiVersion: v2
name: weather-forecast
description: A helm chart for deploying the weather forecast application.
type: application
version: 0.1.0
```

- Contains the metadata for the chart

- Key fields here are the chart name and chart version

### values.yaml
```yaml
# values.yaml

deployment:
  replicaCount: 2
  image:
    repository: docker.io/ericlammers
    name: weather-forecast-frontend
    tag: dev
    pullPolicy: Always
  container:
    port: 3000
service:
  type: ClusterIP
  port: 3000
appConfiguration:
  panelColor: "#343447"
```

- These are the default values for the chart

- These defaults can be overridden with user supplied values

- Helm injects these values into the object templates

### .helmignore
```
# .helmignore

.git
temp/
```

- Lists all the files to ignore when packaging a chart, similiar to .gitignore

### templates/

- Contains the kubernetes object you are deploying with the chart

- Takes all the object templates in this folder and combines it with configured values to create the final kubernetes objects

### templates/_helpers.tpl
```tpl
<!-- Example result: weather-forecast-0.1.0 -->
{{- define "weather-forecast.chart" -}}
{{- printf "%s-%s" .Chart.Name .Chart.Version | replace "+" "_" | trunc 63 | trimSuffix "-" }}
{{- end }}

<!-- Example result: weather-forecast-backend-deployment -->
{{- define "weather-forecast.backend-deployment" -}}
{{- printf "%s-backend-deployment" .Chart.Name }}
{{- end }}
```

- You can optionally include one or more template files that contain functions that can be used throughout the chart

- The example file contains two functions for generating the chart deployment names

### templates/{KUBERNETES_OBJECT}.yaml
```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: {{ include "weather-forecast.backend-deployment" . }}
  labels:
    app: {{ include "weather-forecast.backend" . }}
    helm.sh/chart: {{ include "weather-forecast.chart" . }}
spec:
  replicas: {{ .Values.backend.deployment.replicaCount }}
  selector:
    matchLabels:
      app: {{ include "weather-forecast.backend" . }}
  template:
    metadata:
      annotations:
        backend-appsettings-configmap-checksum" . }}
      labels:
        app: {{ include "weather-forecast.backend" . }}
        helm.sh/chart: {{ include "weather-forecast.chart" . }}
    spec:
      containers:
      - name: {{ include "weather-forecast.backend" . }}
        image: {{ include "weather-forecast.backend-image" . }}
        ports:
        - containerPort: {{ .Values.backend.deployment.container.port }}
```

- All kubernetes object yaml files for the chart are placed in the templates folder

- The object definitions must match the kubernetes spec 

- The templating language and the configured values to dynamically create the objects


### templates/NOTES.txt
```
Thanks for installing the {{ include "weather-forecast.chart" . }} helm chart.

To connect to the application using port forwarding run:
    - kubectl port-forward services/{{ include "weather-forecast.backend-service" . }} {{ .Values.backend.service.port }}
```

- Provides instructions to chart users after chart install or upgrades

## Subcharts
- In a helm chart you can optionally include subcarts Charts/ folder

- This is useful if your chart depends on other charts

- It sometimes makes sense to have a main umbrella chart whose main job is to install the subcharts

- The parent chart can be configured to allow enabling or disabling of subcharts

- You can also reference external charts as dependencies of a helm chart

## Helm chart repositories
- Helm charts can be installed from a local directory or reference a remote helm chart repository

- A helm repository stores packaged charts

- Beneficial to package and store each version of your helm chart

## Third party helm charts

- A Key benefit of helm is the ability to install third party helm charts

- Lets us leave the deployment details to the experts

- We've leveraged the Prometheus helm chart in a couple of our clusters

## Useful helm commands
```
helm install RELEASE_NAME CHART_LOCATION  // Installs a helm chart
```

```
helm upgrade RELEASE_NAME CHART_LOCATION  // Upgrades a helm chart
```

```
helm ls                       // Lists all installed helm charts
```

```
helm get values RELEASE_NAME  // Returns the user supplied values file
```

```
helm history RELEASE_NAME     // Shows the history of releases
```

```
helm rollback RELEASE_NAME     // Rolls the a release back to it's previous revision
```

```
helm rollback RELEASE_NAME     // Rolls the a release back to it's previous revision
```

```
helm repo add REPO_NAME REPO_URL // Adds a chart repository
```

# Demo

### 1. Install the chart
a. Move into the `.helm` folder
```
cd .helm
```

b. Before deploying the chart do a test run to see the generated Kubernetes object
```
helm install --dry-run weather-forecast -f user-supplied-values.yaml ./weather-forecast
```

c. Before deploying the chart do a test run to see the generated Kubernetes object
```
helm install weather-forecast -f user-supplied-values.yaml ./weather-forecast
```

d. List the deployed helm charts
```
helm ls
```

e. Check that the pods are running
```
kubectl get pods 
```

f. See the full list of objects
```
kubectl get all -l helm.sh/chart=weather-forecast-0.1.0
```

g. Get the details for connecting to the application
```
helm get notes weather-forecast
```

### 2. Update the chart configuration
a. Change the UI panel color to dark red in the user-supplied-values.yaml

b. Run the upgrade with the updated user supplied values
```
helm upgrade weather-forecast -f user-supplied-values.yaml ./weather-forecast
```

c. Check the chart history and we should see we are on revision 2
```
helm history weather-forecast
```

d. Not happy with the color choice so we rollback the release
```
helm rollback weather-forecast
```

### 3. Upgrade to a new application version
a. Change the tags in the user-supplied-values.yaml file to 1.0.0 to deploy the newest version of the app (also don't forget to recomment out the panel color change)

b. Run the upgrade with the updated user supplied values
```
helm upgrade weather-forecast -f user-supplied-values.yaml ./weather-forecast
```

### 4. Install the prometheus third party helm chart
(Prometheus helm chart README: https://github.com/helm/charts/tree/master/stable/prometheus)

a. Add the prometheus chart repository to helm
```
helm repo add prometheus-community https://prometheus-community.github.io/helm-charts
```

b. Install the Prometheus helm chart
```
helm install prometheus prometheus-community/prometheus
```

c. List the charts
```
helm ls
```

d. Take a look at the pods
```
kubectl get pods
```

e. Optionally port-forward to the Prometheus UI
```
kubectl port-forward PROMETHEUS_SERVER_POD_NAME 9090
```

